import axios from './index'

export const fetchSimulation = async (queryParams) => {
  const defaultUrl = 'simulation'
  let fullUrl
  const { origin, destination, minutes } = queryParams

  if (queryParams.plan) {
    fullUrl = `${defaultUrl}?origin=${origin}&destination=${destination}&minute=${minutes}&namePlan=${queryParams.plan}`
  } else {
    fullUrl = `${defaultUrl}?origin=${origin}&destination=${destination}&minute=${minutes}`
  }

  return await axios.get(fullUrl)
}
