import axios from 'axios'

const instanceAxios = axios.create({
  baseURL: 'http://localhost:3009'
})

export default instanceAxios
